﻿using EventBusClient;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging.Console;
using Newtonsoft.Json.Linq;
using EventBusClient.Messages;
using EventBusClient.Enums;
using Newtonsoft.Json;

namespace EventBusTester
{
    class Program
    {
        const string CLOSE = "close";

        static EventBusClient.EventBusClient Client;

        static ILogger Logger;

        static void Main(string[] args)
        {
            var loggerFactory = new LoggerFactory();
            loggerFactory.AddProvider(new ConsoleLoggerProvider((text, level) => level >= LogLevel.Debug, true));
            var logger = Logger = loggerFactory.CreateLogger<Program>();
            Console.WriteLine("Starting Event Bus Tester");
            Client = new EventBusClient.EventBusClient(loggerFactory.CreateLogger<EventBusClient.EventBusClient>())
            {
                AuthorizationToken = "",
                Host = "localhost",
                Port = 55124,
                ServiceUuid = Guid.NewGuid()
            };
            logger.LogInformation("Connecting to Event Bus Server");
            Client.ConnectAsync().
                ContinueWith(async (prev) =>
                {
                    logger.LogInformation("Sending initial subscribe message");
                    await Client.SubscribeAsync(1000);
                }).Wait();
            logger.LogInformation("Connected");

            Client.OnEventMessageReceive += Echo;
            Client.AcceptEvents();

            while (true)
            {
                Console.WriteLine("Ready to test. Valid options are \"close\", \"subscribe\", \"send\"");
                var input = Console.ReadLine();
                switch (input.ToLower())
                {
                    case "close":
                        {
                            logger.LogInformation("Closing");
                            Client.CloseAsync().Wait();
                            return;
                        }
                    case "subscribe":
                        {
                            Console.WriteLine("Enter message type [int]");
                            var msgStr = Console.ReadLine();
                            var msgType = int.Parse(msgStr);
                            Client.SubscribeAsync(msgType).Wait();
                            break;
                        }
                    case "send":
                        {
                            var msg = new TestMessage()
                            {
                                MessageUuid = Guid.NewGuid(),
                                OriginServiceUuid = Guid.NewGuid(),
                                Severity = Severity.Info,
                                Time = DateTime.Now,
                                Payload = "Test message"
                            };
                            Client.SendEventAsync(msg).Wait();
                            break;
                        }
                    default:
                        Console.WriteLine("Unknown option");
                        break;
                }
            }
        }

        static void Echo(object sender, EventBusMessageArgs args)
        {
            string strRepresentation = "Received unknown type";
            if (args.PayloadType == typeof(JObject))
            {
                strRepresentation = ((JObject)args.Payload).ToString();
            }
            Logger.LogInformation("Received event:");
            Logger.LogInformation(strRepresentation);
        }
    }

    class TestMessage : IEventBusMessage
    {
        public Guid MessageUuid { get; set; }

        public Guid OriginServiceUuid { get; set; }

        public Severity Severity { get; set; }

        public int MessageType => 9001;

        public DateTime Time { get; set; }

        public object Payload { get => InternalPayload; set => InternalPayload = (string)value; }

        [JsonIgnore]
        public string InternalPayload { get; set; }
    }
}
